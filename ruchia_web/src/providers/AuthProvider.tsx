import React, { useState } from "react"
import { AuthService } from "../services/api/AuthService"
import { LoginResponse } from "../models/LoginResponse"

interface IProps {
	children: React.ReactNode
}

interface AuthContextType {
	isLogged: boolean
	login: (login: string, psw: string) => Promise<LoginResponse>
}

const AuthContext = React.createContext<AuthContextType>({} as AuthContextType)

export function AuthProvider ({ children }: IProps) {
	const [isLogged, setIsLogged] = useState(false)

	const handleLogin = async (login: string, psw: string) => {
		try {
			const response = await AuthService.login(login, psw)
			if (!response.isError) {
				setIsLogged(true)
			}

			return response
		} catch (error) {
			console.error(error)
			const response: LoginResponse = {
				isError: true,
				errorMessage: "Une erreur est survenue lors de la connexion",
				data: ""
			}
			return response
		}
	}

	const value: AuthContextType = {
		login: handleLogin,
		isLogged: isLogged
	}

	return (
		<AuthContext.Provider value={value}>
			{children}
		</AuthContext.Provider>
	)
}

export function useAuth () {
	return React.useContext(AuthContext)
}

export function useIsLogged () {
	const auth = useAuth()
	return auth.isLogged
}
