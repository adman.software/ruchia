import { ThemeProvider, createTheme } from "@mui/material"
import { BrowserRouter } from "react-router-dom"
import Navigator from "./components/Navigator/Navigator"
import { AuthProvider } from "./providers/AuthProvider"

const theme = createTheme({
  components: {
    MuiButton: {
      defaultProps: {
        variant: "contained",
      }
    },

    MuiTextField: {
      defaultProps: {
        variant: "outlined",
      }
    }
  }
})

function App() {

  return (
    <ThemeProvider  theme={theme}>
        <AuthProvider>
          <BrowserRouter>
            <Navigator />
          </BrowserRouter>
        </AuthProvider>
    </ThemeProvider>
  )
}

export default App
