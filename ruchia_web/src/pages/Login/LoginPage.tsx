import { useState, useEffect } from 'react'
import { Stack, Typography, TextField, Button } from '@mui/material'
import Page from '../../components/Page/Page'
import { useAuth } from '../../providers/AuthProvider'
import { useLocation, useNavigate } from 'react-router-dom'

function LoginPage() {
    const [email, setEmail] = useState("")
	const [psw, setPsw] = useState("")

    const { login, isLogged } = useAuth()
    const location = useLocation()
    const navigate = useNavigate()

    useEffect(() => {
		if (isLogged) {
			let to = "/"
			if (location.state) {
				const { from } = location.state as { from: string }
				if (from) {
					to = from
				}
			}
			navigate(to)
		}
	}, [isLogged, location, navigate])

    const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		setEmail(e.target.value)
	}

	const handlePswChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		setPsw(e.target.value)
	}

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault()
        await login(email, psw)
	}

  return (
    <Page title="Ruchia" container="md">
        <Stack justifyContent="center" alignItems="center" height="100%">
            <form onSubmit={handleSubmit}>
                <Stack spacing={2}>
                    <Typography variant="h5" textAlign="center">Connexion</Typography>
                    <TextField 
                        type="email" 
                        label="Email" 
                        value={email}
                        onChange={handleEmailChange}
                    />
                    <TextField 
                        type="password" 
                        label="Mot de passe"
                        value={psw}
                        onChange={handlePswChange}
                    />
                    <Button type="submit">Valider</Button>
                </Stack>
            </form>
        </Stack>
    </Page>
  )
}

export default LoginPage