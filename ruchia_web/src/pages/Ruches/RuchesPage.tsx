import { useState, useEffect } from 'react'
import Page from '../../components/Page/Page'
import { useLocation, useNavigate } from 'react-router-dom'
import { Rucher } from '../../models/Rucher'
import { Ruche } from '../../models/Ruche'
import { RucheService } from '../../services/api/RucheService'
import RucheItem from './RucheItem'
import { RucherService } from '../../services/api/RucherService'
import { Fab } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'

function RuchesPage() {
    const [ruches, setRuches] = useState<Ruche[]>([])

    const navigate = useNavigate()

    const location = useLocation()
    const rucher = location.state as Rucher

    useEffect(() => {
        fetchRuches()
    }, [rucher])

    const fetchRuches = async () => {
        const fetchedRuches: Ruche[] = []
        for (const rucheId in rucher.ruches) {
            const resp = await RucheService.getById(rucheId)
            if (!resp.isError) {
                fetchedRuches.push(resp.data)
            }
        }
        setRuches(fetchedRuches)
    }

    const handleAdd = () => {
        navigate("/newruche", { state: rucher.id })
    }

    const handleUpdate = (value: Ruche) => {
        const index = ruches.findIndex((ruche) => ruche.id === value.id)
        const newRuches = [...ruches]
        newRuches[index] = value
        setRuches(newRuches)
    }

    const handleRemoveHiveFromRucher = async (hiveId: string) => {
        const resp = await RucherService.removeHiveFromRucher(rucher.id, hiveId)
        if (!resp.isError) {
            const newRuches = ruches.filter((ruche) => ruche.id !== hiveId)
            setRuches(newRuches)
        }
    }

  return (
    <Page title={`Ruches de ${rucher.nom}`} container='lg'>
        {ruches.map((ruche) => <RucheItem key={ruche.id} item={ruche} onUpdate={handleUpdate} onDelete={handleRemoveHiveFromRucher} />)}
        <Fab
        color="primary" 
        aria-label="add"
        sx={{
          position: "fixed",
          bottom: "20px",
          right: "20px"
        }}
        onClick={handleAdd}
      >
        <AddIcon />
      </Fab>
    </Page>
  )
}

export default RuchesPage