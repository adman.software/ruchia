import { Checkbox, FormControlLabel, IconButton, Paper, Stack, Typography } from "@mui/material"
import DeleteIcon from '@mui/icons-material/Delete'
import { ChangeEvent } from "react"
import { Ruche } from "../../models/Ruche"
import { RucheService } from "../../services/api/RucheService"

type IProps = {
    item: Ruche
    onUpdate: (value: Ruche) => void
    onDelete: (id: string) => void
}

function RucheItem({ item, onUpdate, onDelete }: IProps) {

    const handleToogleAlert = async (event: ChangeEvent<HTMLInputElement>) => {
        const isChecked = event.target.checked
        const resp = await RucheService.update(item.id, item.nom, isChecked)
        if (!resp.isError) {
            onUpdate({
                ...item,
                alerte_ouverture: isChecked
            })
        }
    }

    const handleDelete = async () =>{
        onDelete(item.id)
    }

  return (
    <Paper sx={{ p: 2 }}>
        <Stack>
            <Stack direction="row" justifyContent="space-between">
                <Typography variant="h6">{ item.nom }</Typography>
                <IconButton onClick={handleDelete}>
                    <DeleteIcon />
                </IconButton>
            </Stack>
            <Stack direction="row" justifyContent="space-between">
                <Stack>
                    <Typography variant="body1" fontWeight="bold" textAlign="center">Température :</Typography>
                    <Typography variant="body1" textAlign="center">{item.temperature}</Typography>
                </Stack>
                <Stack>
                    <Typography variant="body1" fontWeight="bold" textAlign="center">Humidité :</Typography>
                    <Typography variant="body1" textAlign="center">{item.humidite}</Typography>
                </Stack>
                <Stack>
                    <Typography variant="body1" fontWeight="bold" textAlign="center">Couvercle :</Typography>
                    <Typography variant="body1" textAlign="center">
                        {!item.couvercle_ferme ? 'Ouvert' : 'Fermé'}
                    </Typography>
                </Stack>
            </Stack>
            <FormControlLabel
                label="Activer les alertes"
                control={
                    <Checkbox
                        checked={item.alerte_ouverture}
                        onChange={handleToogleAlert}
                    />
                }
            />
        </Stack>
    </Paper>
  )
}

export default RucheItem