import { Fab, Stack } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
import { useState, useEffect } from 'react'
import Page from '../../components/Page/Page'
import { Rucher } from '../../models/Rucher'
import { RucherService } from '../../services/api/RucherService'
import RucherItem from './RucherItem'
import { useNavigate } from 'react-router-dom'

function HomePage() {
  const [ruchers, setRuchers] = useState<Rucher[]>([])

  const navigate = useNavigate()

  useEffect(() => {
    fetchRuchers()
  }, [])

  const fetchRuchers = async () => {
    try {
      const response = await RucherService.getAll()
      if (!response.isError) {
        setRuchers(response.data)
      }
    } catch (error) {
      console.error(error)
    }
  }

  const handleDelete = async (id: string) => {
    try {
      const response = await RucherService.delete(id)
      if (!response.isError) {
        setRuchers(ruchers.filter((rucher) => rucher.id !== id))
      }
    } catch (error) {
      console.error(error)
    }
  }

  const handleAdd = () => {
    navigate("/newrucher")
  }

  return (
    <Page title="Liste des ruchers" container="md">
      <Stack spacing={2}>
        {ruchers.map((rucher) => (
          <RucherItem key={rucher.id} item={rucher} onDelete={handleDelete} />
        ))}
      </Stack>
      <Fab 
        color="primary" 
        aria-label="add"
        sx={{
          position: "fixed",
          bottom: "20px",
          right: "20px"
        }}
        onClick={handleAdd}
      >
        <AddIcon />
      </Fab>
    </Page>
  )
}

export default HomePage