import { Stack, Typography, IconButton, Paper, Button } from '@mui/material'
import DeleteIcon from '@mui/icons-material/Delete'
import { Rucher } from '../../models/Rucher'
import { useNavigate } from 'react-router-dom'

type IProps = {
    item: Rucher
    onDelete: (id: string) => void
}

function RucherItem({ item, onDelete }: IProps) {
    const navigate = useNavigate()

    const handleDelete = () => {
        onDelete(item.id)
    }

    const handleVisualiser = () => {
        navigate('/ruches', { state: item })
    }

  return (
    <Paper>
        <Stack spacing={2} p={2}>
            <Stack direction="row" justifyContent="space-between">
                <Typography variant="h6">{item.nom}</Typography>
                <IconButton onClick={handleDelete}>
                    <DeleteIcon />
                </IconButton>
            </Stack>
            <Typography variant="body2">{item.adresse}</Typography>
            <Typography variant="body1">{item.description}</Typography>
            <Stack direction="row" justifyContent="flex-end">
                <Button onClick={handleVisualiser}>Visualiser</Button>
            </Stack>
        </Stack>
    </Paper>
  )
}

export default RucherItem