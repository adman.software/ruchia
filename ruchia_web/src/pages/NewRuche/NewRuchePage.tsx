import { useState } from 'react'
import { Stack, Typography, TextField, Button } from '@mui/material'
import Page from '../../components/Page/Page'
import { RucherService } from '../../services/api/RucherService'
import { useLocation, useNavigate } from 'react-router-dom'
import { RucheService } from '../../services/api/RucheService'

function NewRuchePage() {
    const [nom, setNom] = useState("")
    const [id, setId] = useState("")

    const navigate = useNavigate()

    const location = useLocation()
    const rucherId = location.state as string

    const handleNomChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setNom(e.target.value)
    }

    const handleIdChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setId(e.target.value)
    }


    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        await RucheService.update(id, nom, true)
        const response = await RucherService.addHiveToRucher(rucherId, id)
        if (!response.isError) {
            navigate("/")
        }
    }

  return (
    <Page title="Ruchia" container="md">
        <Stack justifyContent="center" alignItems="center" height="100%">
            <form onSubmit={handleSubmit}>
                <Stack spacing={2}>
                    <Typography variant="h5" textAlign="center">Ajouter une ruche</Typography>
                    <TextField 
                        label="Nom" 
                        value={nom}
                        onChange={handleNomChange}
                    />
                    <TextField 
                        label="Id de la ruche"
                        value={id}
                        onChange={handleIdChange}
                    />
                    <Button type="submit">Valider</Button>
                </Stack>
            </form>
        </Stack>
    </Page>
  )
}

export default NewRuchePage