import { useState } from 'react'
import { Stack, Typography, TextField, Button } from '@mui/material'
import Page from '../../components/Page/Page'
import { RucherService } from '../../services/api/RucherService'
import { useNavigate } from 'react-router-dom'

function NewRucherPage() {
    const [nom, setNom] = useState("")
    const [adresse, setAdresse] = useState("")
    const [description, setDescription] = useState("")

    const navigate = useNavigate()

    const handleNomChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setNom(e.target.value)
    }

    const handleAdresseChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setAdresse(e.target.value)
    }

    const handleDescriptionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setDescription(e.target.value)
    }

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        const response = await RucherService.add(nom, adresse, description)
        if (!response.isError) {
            navigate("/")
        }
    }

  return (
    <Page title="Ruchia" container="md">
        <Stack justifyContent="center" alignItems="center" height="100%">
            <form onSubmit={handleSubmit}>
                <Stack spacing={2}>
                    <Typography variant="h5" textAlign="center">Ajouter un rucher</Typography>
                    <TextField 
                        label="Nom" 
                        value={nom}
                        onChange={handleNomChange}
                    />
                    <TextField 
                        label="Adresse"
                        value={adresse}
                        onChange={handleAdresseChange}
                    />
                    <TextField 
                        label="Description"
                        value={description}
                        multiline
                        onChange={handleDescriptionChange}
                    />
                    <Button type="submit">Valider</Button>
                </Stack>
            </form>
        </Stack>
    </Page>
  )
}

export default NewRucherPage