import { RequestResponse } from "./RequestResponse"

export type Ruche = {
    id: string
    nom: string
    apiculteurId: string
    alerte_ouverture: boolean
    couvercle_ferme: boolean
    humidite: string
    temperature: string
}

export type RucheResponse = RequestResponse<Ruche>
export type RucheListResponse = RequestResponse<Ruche[]>