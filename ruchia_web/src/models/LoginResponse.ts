import { RequestResponse } from "./RequestResponse"

export type LoginResponse = RequestResponse<string>