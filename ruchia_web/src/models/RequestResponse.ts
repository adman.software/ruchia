export type RequestResponse<T> = {
    isError: boolean
    errorMessage: string | null
    data: T
}

export type EmptyResponse = RequestResponse<null>