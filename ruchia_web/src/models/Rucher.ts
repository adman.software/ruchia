import { RequestResponse } from "./RequestResponse"

export type Rucher = {
    id: string
    nom: string
    adresse: string
    description: string
    apiculteurId: string
    ruches: Record<string, string>
}

export type RucherResponse = RequestResponse<Rucher>
export type RucherListResponse = RequestResponse<Rucher[]>