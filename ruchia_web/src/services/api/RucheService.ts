import { RucheResponse } from "../../models/Ruche";
import { AxiosConfigurator } from "./AxiosConfigurator";

function getAxios () {
	return AxiosConfigurator.getInstance("ruche", false)
}

export class RucheService {

    public static async getById (id: string) {
        const axios = getAxios()
        const response = await axios.get<RucheResponse>(`/${id}`)
        return response.data
    }

    public static async update (id: string, nom: string, alert: boolean) {
        const axios = getAxios()
        const response = await axios.put<RucheResponse>(`/${id}`, { 
            nom, 
            alerte_ouverture: alert 
        })
        return response.data
    }

}