import { LoginResponse } from "../../models/LoginResponse";
import { AxiosConfigurator } from "./AxiosConfigurator";

function getAxios (formData = false) {
	return AxiosConfigurator.getInstance("auth", formData)
}

export class AuthService {
    
    public static async login (email: string, psw: string) {
        const axios = getAxios()
        const response = await axios.post<LoginResponse>("", { 
            email: email,
            mot_de_passe: psw
         })

         if (!response.data.isError) {
            localStorage.setItem("token", response.data.data)
        }

        return response.data
    }

}