import { EmptyResponse } from "../../models/RequestResponse";
import { RucherListResponse } from "../../models/Rucher";
import { AxiosConfigurator } from "./AxiosConfigurator";

function getAxios (formData = false) {
	return AxiosConfigurator.getInstance("rucher", formData)
}

export class RucherService {
    
    public static async getAll () {
        const axios = getAxios()
        const response = await axios.get<RucherListResponse>("")
        return response.data
    }

    public static async add (nom: string, adresse: string, description: string) {
        const axios = getAxios()
        const response = await axios.post<EmptyResponse>("", { nom, adresse, description })
        return response.data
    }

    public static async delete (id: string) {
        const axios = getAxios()
        const response = await axios.delete<EmptyResponse>(`/${id}`)
        return response.data
    }

    public static async addHiveToRucher (rucherId: string, rucheId: string) {
        const axios = getAxios()
        const response = await axios.put<EmptyResponse>(`/addruche/${rucherId}/${rucheId}`)
        return response.data
    }

    public static async removeHiveFromRucher (rucherId: string, rucheId: string) {
        const axios = getAxios()
        const response = await axios.delete<EmptyResponse>(`/removeruche/${rucherId}/${rucheId}`)
        return response.data
    }

}