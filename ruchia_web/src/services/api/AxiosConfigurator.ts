import axios, { AxiosRequestConfig, AxiosResponse } from "axios"
import { RequestResponse } from "../../models/RequestResponse"

const baseURL = import.meta.env.VITE_SERVER_URL

/**
 * Defines global axios configuration
 */
export class AxiosConfigurator {
	static config: AxiosRequestConfig = {
		withCredentials: true,
		headers: {
			"Content-Type": "application/json",
			"Accept": "application/json",
			"Authorization": `Bearer ${localStorage.getItem("token")}`
		}
	}

	static formDataConfig: AxiosRequestConfig = {
		...AxiosConfigurator.config,
		headers: {
			...AxiosConfigurator.config.headers,
			"Content-Type": "multipart/form-data"
		}
	}

	/**
	 * Gets a configured Axios instance
	 * 
	 * @param {string} endpoint The service api end point
	 * @param {boolean | undefined} isFormData Whether the request is a form data request
	 * @returns {AxiosInstance} The Axios instance
	 */
	static getInstance (endpoint: string, isFormData = false) {
		const config = isFormData ? AxiosConfigurator.formDataConfig : AxiosConfigurator.config
		const abortController = new AbortController()

		const axiosInstance =  axios.create({
			...config,
			baseURL: `${baseURL}/${endpoint}`,
			signal: abortController.signal
		})

		axiosInstance.interceptors.response.use(
			(response: AxiosResponse) => response,
			(({ response }: { response: AxiosResponse<RequestResponse<null>> }) => {
				if (response?.data?.isError) {
					if (response.data.errorMessage === "La session a expirée !") {
						window.location.reload()
					}

					return Promise.reject(response.data.errorMessage)
				} else {
					return Promise.reject(response)
				}
			})
		)

		return axiosInstance
	}
}