import React from "react"
import { Routes, Route } from "react-router-dom"
import HomePage from "../../pages/Home/HomePage"
import LoginPage from "../../pages/Login/LoginPage"
import { ProtectedRoute } from "./ProtectedRoute"
import NewRucherPage from "../../pages/NewRucher/NewRucherPage"
import RuchesPage from "../../pages/Ruches/RuchesPage"
import NewRuchePage from "../../pages/NewRuche/NewRuchePage"

function Navigator () {
	return (
		<React.Fragment>
			<Routes>
				<Route path="/" element={<ProtectedRoute><HomePage /></ProtectedRoute>} />
				<Route path="/login" element={<LoginPage />} />
				<Route path="/newrucher" element={<ProtectedRoute><NewRucherPage /></ProtectedRoute>} />
				<Route path="/ruches" element={<ProtectedRoute><RuchesPage /></ProtectedRoute>} />
				<Route path="/newruche" element={<ProtectedRoute><NewRuchePage /></ProtectedRoute>} />
			</Routes>
		</React.Fragment>
	)
}

export default Navigator
