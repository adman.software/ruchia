import React from "react"
import { Navigate, useLocation } from "react-router-dom"
import { useAuth } from "../../providers/AuthProvider";

interface IProps {
	children: React.ReactElement;
}

export function ProtectedRoute ({ children }: IProps) {
	const { isLogged } = useAuth()
	const location = useLocation()

	if (!isLogged) {
		return <Navigate to="/login" state={{ from: location.pathname }} />
	}

	return children
}
