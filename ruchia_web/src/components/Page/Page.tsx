import React from 'react'
import { Box, AppBar, Toolbar, Typography, Stack, Container, Breakpoint } from '@mui/material'

type IProps = {
    children: React.ReactNode
    container: Breakpoint
	title: string
}

function Page({ children, container, title }: IProps) {
  return (
    <Box flex={1} height="100vh" overflow="hidden">
			<AppBar position="relative">
				<Toolbar>
					<Typography flexGrow={1} variant="h5" ml={2}>{title}</Typography>
				</Toolbar>
			</AppBar>
			<Box
				sx={{
					width: "100%",
					height: "100%",
				}}
			>
				<Stack>
					<Stack p={2} height="100vh" overflow="auto">
						<Container maxWidth={container}>
							{children}
						</Container>
					</Stack>
				</Stack>
			</Box>
		</Box>
  )
}

export default Page