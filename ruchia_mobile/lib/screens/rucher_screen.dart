import 'package:flutter/material.dart';
import 'package:ruchia_mobile/components/ewRucherForm.dart';
import 'package:ruchia_mobile/models/req_response.dart';
import 'package:ruchia_mobile/models/rucher.dart';
import 'package:ruchia_mobile/services/rucher_service.dart';
import 'package:ruchia_mobile/components/rucher_card.dart';

class RucherScreen extends StatefulWidget {
  RucherScreen({Key? key}) : super(key: key);

  @override
  _RucherScreenState createState() => _RucherScreenState();
}

class _RucherScreenState extends State<RucherScreen> {
  RucherService rucherService = RucherService();

  late Future<ReqResponse> futureRuchers;

  @override
  void initState() {
    super.initState();
    futureRuchers = rucherService.getRuchers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Liste des Ruchers'),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final result = await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => NewRucherForm()),
          );

          // Rafraîchir la liste des ruchers si un nouveau rucher a été ajouté
          if (result != null && result == true) {
            setState(() {
              futureRuchers = rucherService.getRuchers();
            });
          }
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.yellow,
  ),
      body: FutureBuilder<ReqResponse>(
      future: RucherService().getRuchers(),
      builder: (BuildContext context, AsyncSnapshot<ReqResponse> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        } else {
          if (snapshot.hasError) {
            return Text('Erreur : ${snapshot.error}');
          } else {
            return ListView.builder(
              itemCount: snapshot.data!.data.length,
              itemBuilder: (ctx, i) => RucherCard
              (
                rucher: snapshot.data!.data[i],
                onDelete: () async {
                  try {
                    ReqResponse response = await rucherService.deleteRucher(snapshot.data!.data[i].id);
                    if (!response.isError) {
                      setState(() {
                        snapshot.data!.data.removeAt(i);
                      });
                    } else {
                      // afficher une erreur à l'utilisateur
                    }
                  } catch (e) {
                    // Gérer ou signaler l'erreur
                  }
                },
              ),
            );
          }
        }
      },
    ),
    );
  }
}
