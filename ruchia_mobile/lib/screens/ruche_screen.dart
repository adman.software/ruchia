import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ruchia_mobile/components/ruche_card.dart';
import 'package:ruchia_mobile/models/ruche.dart';
import 'package:ruchia_mobile/models/rucher.dart';
import 'package:ruchia_mobile/screens/new_ruche_screen.dart';
import 'package:ruchia_mobile/services/ruche_service.dart';

class RuchesScreen extends StatefulWidget {
  final Rucher rucher;

  RuchesScreen({required this.rucher});

  @override
  _RuchesScreenState createState() => _RuchesScreenState();
}

class _RuchesScreenState extends State<RuchesScreen> {
  List<Ruche> ruches = [];

  @override
  void initState() {
    super.initState();
    fetchRuches();
  }
  
  Future<void> fetchRuches() async {
    for (String rucheId in widget.rucher.ruches.values) {
      final response = await RucheService().getRucheById(rucheId);
      if (response.data != null) {
        print(response.data);
        setState(() {
          ruches.add(response.data!);
        });
      }
    }
  }

  void removeHiveFormRucher(String rucheId) async {
    final response = await RucheService().removeRucheFromRucher(widget.rucher.id, rucheId);
    if (response.isError) {
      return;
    }

    setState(() {
      ruches.removeWhere((ruche) => ruche.id == rucheId);
    });
  }


  @override
  Widget build(BuildContext context) {
    Rucher rucher = widget.rucher;

    return Scaffold(
      appBar: AppBar(
        title: Text('Ruches de ${rucher.nom}'),
      ),
      body: Container(
        child: ListView.builder(
          itemCount: ruches.length,
          itemBuilder: (ctx, i) => RucheCard(ruche: ruches[i], onDeleteRuche: removeHiveFormRucher,),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final result = await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => NewRucheScreen(rucherId: rucher.id)),
          );

          if (result != null && result == true) {
            fetchRuches();
          } 
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.yellow,
  ),
    );
  }

}