import 'package:flutter/material.dart';
import 'package:ruchia_mobile/models/ruche.dart';
import 'package:ruchia_mobile/services/ruche_service.dart';

class NewRucheScreen extends StatefulWidget {
  final String rucherId;

  NewRucheScreen({required this.rucherId});

  @override
  _NewRucheScreenState createState() => _NewRucheScreenState();
}

class _NewRucheScreenState extends State<NewRucheScreen> {
  final _formKey = GlobalKey<FormState>();
  String _rucheId = '';
  String _rucheNom = '';

  void _submitForm() async {
    if (_formKey.currentState!.validate()) {
      // Envoyer les données au service pour créer la nouvelle ruche
      final rucheService = RucheService();
      await rucheService.addRuche(widget.rucherId, _rucheId);
      
      final ruche = Ruche(id: _rucheId, nom: _rucheNom, apiculteurId: "", alerte_ouverture: true, couvercle_ferme: false, humidite: "0", temperature: "0");
      await rucheService.updateRuche(_rucheId, ruche);
      
      // Retourner à l'écran précédent
      Navigator.pop(context, true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nouvelle Ruche'),
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Identifiant de la Ruche',
                  ),
                  onChanged: (value) {
                    setState(() {
                      _rucheId = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Veuillez saisir l\'identifiant de la ruche';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Nom de la Ruche',
                  ),
                  onChanged: (value) {
                    setState(() {
                      _rucheNom = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Veuillez saisir le nom de la ruche';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                ElevatedButton(
                  onPressed: _submitForm,
                  child: Text(
                    'Valider',
                    style: TextStyle(fontSize: 20),
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.blue,
                    padding: EdgeInsets.symmetric(horizontal: 40, vertical: 16),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
