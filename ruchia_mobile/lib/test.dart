import 'package:http/http.dart' as http;

void main() async {
  final String baseUrl = "http://localhost:8080/api";
  final String token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjhkMDNhZTdmNDczZjJjNmIyNTI3NmMwNjM2MGViOTk4ODdlMjNhYTkiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vcnVjaGlhLTQyZTAxIiwiYXVkIjoicnVjaGlhLTQyZTAxIiwiYXV0aF90aW1lIjoxNjg3MzQ1MjU1LCJ1c2VyX2lkIjoiN1I0WG1mYXJjbVJaUlh5bDB3TTd1MEFHa1ltMiIsInN1YiI6IjdSNFhtZmFyY21SWlJYeWwwd003dTBBR2tZbTIiLCJpYXQiOjE2ODczNDUyNTUsImV4cCI6MTY4NzM0ODg1NSwiZW1haWwiOiJhZGFtLmF0bWFuaW91QHlhaG9vLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJhZGFtLmF0bWFuaW91QHlhaG9vLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.oriigfVrcEOKUPxmaXsUVYmaI0mNopu1klFvlYpSbpywqaujpa_OHypHu2NsjZY7XcKUOVKTltU2j1OW69sLvNM-506vl-HrCcBvAiLx2eZBkzVcLhxEDXgykz9zR324UrLFXRRx8vMPeNvgz6DpqsC112j9wfBhuyCJxQz3Q2KDNvGU0jlHGiPUMkLvYnL-QlGTIhmmS4zdTojWpXVBKjgO7ezbeQyjPmLIPBmt0DuKhBajG9pHAo0myufMm1pXl7kuWqHWd8MAZ5fbj4_inkS5UrWAXpfKxRW0UPnFBer8FCtKNPvBjJep_MvuAKx2lhsMpO9iNxDbD1Roj4XgjA"; // Votre token ici
  final response = await http.get(
    Uri.parse('$baseUrl/rucher'),
    headers: {
      "Authorization": "Bearer $token",
      "Content-Type": "application/json",
    },
  );
  print('Response status: ${response.statusCode}');
  print('Response body: ${response.body}');
}
