import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ruchia_mobile/screens/login_screen.dart';
import 'package:ruchia_mobile/screens/rucher_screen.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => LoginScreen(),
        '/rucher':(context) => RucherScreen(),
      },
    );
  }
}
