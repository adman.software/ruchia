import 'dart:convert';

import 'package:ruchia_mobile/models/req_response.dart';
import 'package:ruchia_mobile/models/rucher.dart';
import 'package:ruchia_mobile/services/api_service.dart';

class RucherService extends ApiService {
  Future<ReqResponse<List<Rucher>>> getRuchers() async {
    final response = await client.get(
      Uri.parse('$baseUrl/rucher'),
    );

    final reqResponse = ReqResponse.fromJson(jsonDecode(response.body), (data) {
      var list = data as List;
      List<Rucher> rucherList = list.map((i) => Rucher.fromJson(i)).toList();
      return rucherList;
    });
    return reqResponse;
  }

  Future<ReqResponse> addRucher(String nom, String adresse, String description) async {
    Map<String, dynamic> rucherData = {
      'nom': nom,
      'adresse': adresse,
      'description': description
    };
    final response = await client.post(
      Uri.parse('$baseUrl/rucher'),
      headers: {"Content-Type": "application/json"},
      body: jsonEncode(rucherData),
    );

    final reqResponse = ReqResponse.fromJson(jsonDecode(response.body), (data) => null);
    return reqResponse;
  }


  Future<ReqResponse> deleteRucher(String rucherId) async {
    final response = await client.delete(
      Uri.parse('$baseUrl/rucher/$rucherId'),
    );

    final reqResponse = ReqResponse.fromJson(jsonDecode(response.body), (data) => null);
    return reqResponse;
  }
}
