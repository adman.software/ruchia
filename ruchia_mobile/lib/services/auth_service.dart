import 'dart:convert';

import 'package:ruchia_mobile/models/req_response.dart';
import 'package:ruchia_mobile/services/api_service.dart';
import 'package:localstorage/localstorage.dart';

class AuthService extends ApiService {
  static LocalStorage storage = new LocalStorage('ruchia_mobile_storage.json');

  Future<ReqResponse<String>> login(String email, String password) async {
    final response = await client.post(
      Uri.parse('$baseUrl/auth'),
      body: jsonEncode({"email": email, "mot_de_passe": password}),
      headers: {"Content-Type": "application/json"},
    );

    final reqResponse = ReqResponse.fromJson(jsonDecode(response.body), (data) => data as String);
    storage.setItem('logintoken', reqResponse.data);
    return reqResponse;
  }
}