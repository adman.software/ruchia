import 'package:http_interceptor/http_interceptor.dart';
import 'package:localstorage/localstorage.dart';

class ApiInterceptor implements InterceptorContract {
  static LocalStorage storage = LocalStorage('ruchia_mobile_storage.json');

  @override
  Future<RequestData> interceptRequest({required RequestData data}) async {
    await storage.ready;
    String? token = storage.getItem('logintoken');
    if (token != null) {
      data.headers["Authorization"] = "Bearer $token";
    }

    data.headers["Content-Type"] = "application/json";
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({required ResponseData data}) async {
    return data;
  }
}