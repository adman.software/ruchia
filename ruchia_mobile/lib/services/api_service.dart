import 'package:http/http.dart';
import 'package:http_interceptor/http_interceptor.dart';
import 'package:ruchia_mobile/services/api_interceptor.dart';

class ApiService {
  String baseUrl = "http://localhost:8080/api";
  Client client = InterceptedClient.build(interceptors: [
      ApiInterceptor(),
  ]);
}
