
import 'dart:convert';

import 'package:ruchia_mobile/models/req_response.dart';
import 'package:ruchia_mobile/models/ruche.dart';
import 'package:ruchia_mobile/services/api_service.dart';

class RucheService extends ApiService {
  Future<ReqResponse<Ruche>> getRucheById (String rucheId) async {
    final response = await client.get(
      Uri.parse('$baseUrl/ruche/$rucheId'),
    );

    final reqResponse = ReqResponse.fromJson(jsonDecode(response.body), (data) {
      return Ruche.fromJson(data as Map<String, dynamic>);
    });
    return reqResponse;
  }

  Future<ReqResponse> updateRuche(String id, Ruche ruche) async {
    final response = await client.put(
      Uri.parse('$baseUrl/ruche/$id'),
      body: jsonEncode(ruche.toJson()),
    );

    final reqResponse = ReqResponse.fromJson(jsonDecode(response.body), (data) {
      return null;
    });

    return reqResponse;
  }

  Future<ReqResponse> addRuche(String rucherId, String rucheId) async {
    final response = await client.put(
      Uri.parse('$baseUrl/rucher/addruche/$rucherId/$rucheId'),
    );

    final reqResponse = ReqResponse.fromJson(jsonDecode(response.body), (data) {
      return null;
    });

    return reqResponse;
  }

  Future<ReqResponse> removeRucheFromRucher (String rucherId, String rucheId) async {
    final response = await client.delete(
      Uri.parse('$baseUrl/rucher/removeruche/$rucherId/$rucheId'),
    );

    final reqResponse = ReqResponse.fromJson(jsonDecode(response.body), (data) {
      return null;
    });

    return reqResponse;
  }
}
