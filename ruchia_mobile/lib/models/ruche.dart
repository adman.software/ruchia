class Ruche {
  final String id;
  String nom;
  final String apiculteurId;
  bool alerte_ouverture;
  final bool couvercle_ferme;
  final String humidite;
  final String temperature;

  Ruche({
    required this.id,
    required this.nom,
    required this.apiculteurId,
    required this.alerte_ouverture,
    required this.couvercle_ferme,
    required this.humidite,
    required this.temperature,
  });

  // Conversion d'une instance de Ruche en un objet JSON
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'nom': nom,
      'apiculteurId': apiculteurId,
      'alerte_ouverture': alerte_ouverture,
      'couvercle_ferme': couvercle_ferme,
      'humidite': humidite,
      'temperature': temperature,
    };
  }

  // Conversion d'un objet JSON en une instance de Ruche
  factory Ruche.fromJson(Map<String, dynamic> json) {
    return Ruche(
      id: json['id'],
      nom: json['nom'],
      apiculteurId: json['apiculteurId'],
      alerte_ouverture: json['alerte_ouverture'],
      couvercle_ferme: json['couvercle_ferme'],
      humidite: json['humidite'],
      temperature: json['temperature'],
    );
  }
}
