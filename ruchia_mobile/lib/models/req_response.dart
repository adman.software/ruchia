// Modèle ReqResponse
class ReqResponse<T> {
  final bool isError;
  final String? errorMessage;
  final T? data;

  ReqResponse({
    required this.isError,
    required this.errorMessage,
    required this.data,
  });

  factory ReqResponse.fromJson(Map<String, dynamic> json, T Function(Object?) fromJson) {
    return ReqResponse<T>(
      isError: json["isError"],
      errorMessage: json["errorMessage"],
      data: fromJson(json["data"]),
    );
  }
}
