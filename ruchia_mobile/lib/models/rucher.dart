class Rucher {
  final String id;
  final String nom;
  final String adresse;
  final String description;
  final Map<String, String> ruches;
  final String apiculteurId;

  Rucher({
    required this.id,
    required this.nom,
    required this.adresse,
    required this.description,
    required this.ruches,
    required this.apiculteurId,
  });

  // Conversion d'une instance de Rucher en un objet JSON
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'nom': nom,
      'adresse': adresse,
      'description': description,
      'ruches': ruches,
      'apiculteurId': apiculteurId,
    };
  }

  // Conversion d'un objet JSON en une instance de Rucher
  factory Rucher.fromJson(Map<String, dynamic> json) {
    return Rucher(
      id: json['id'],
      nom: json['nom'],
      adresse: json['adresse'],
      description: json['description'],
      ruches: Map<String, String>.from(json['ruches']),
      apiculteurId: json['apiculteurId'],
    );
  }
}
