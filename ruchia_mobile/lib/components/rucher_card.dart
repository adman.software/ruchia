import 'package:flutter/material.dart';
import 'package:ruchia_mobile/models/rucher.dart';
import 'package:ruchia_mobile/screens/ruche_screen.dart';

class RucherCard extends StatelessWidget {
  final Rucher rucher;
  // final VoidCallback onView;
  final VoidCallback onDelete;

  const RucherCard({
    Key? key,
    required this.rucher,
    //required this.onView,
    required this.onDelete,
  }) : super(key: key);

  void gotoRucheScreen (BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => RuchesScreen(rucher: rucher)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  rucher.nom,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: onDelete
                ),
              ],
            ),
            Text(
              rucher.adresse,
              style: TextStyle(color: Colors.grey),
            ),
            SizedBox(height: 10.0),
            Text(
              rucher.description,
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 10.0),
            Align(
              alignment: Alignment.bottomRight,
              child: ElevatedButton(
                child: Text('Visualiser'),
                //onPressed: onView,
                onPressed: (){
                  gotoRucheScreen(context);
                }
              ),
            ),
          ],
        ),
      ),
    );
  }
}
