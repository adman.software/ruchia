import 'package:flutter/material.dart';
import 'package:ruchia_mobile/models/req_response.dart';
import 'package:ruchia_mobile/models/rucher.dart';
import 'package:ruchia_mobile/services/rucher_service.dart';

class NewRucherForm extends StatefulWidget {
  @override
  _NewRucherFormState createState() => _NewRucherFormState();
}

class _NewRucherFormState extends State<NewRucherForm> {
  final _formKey = GlobalKey<FormState>();
  String _nom = '';
  String _adresse = '';
  String _description = '';

  void _submit() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      try {
        ReqResponse resp = await RucherService().addRucher(_nom, _adresse, _description);
      } catch (e) {
        print(e);
      }
      Navigator.pop(context, true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nouveau rucher'),
      ),
      body: Center(
        child: Form(
        key: _formKey,
        child: ListView(
          children: <Widget>[
            TextFormField(
              decoration: InputDecoration(labelText: 'Nom'),
              onSaved: (value) {
                _nom = value!;
              },
            ),
            TextFormField(
              decoration: InputDecoration(labelText: 'Adresse'),
              onSaved: (value) {
                _adresse = value!;
              },
            ),
            TextFormField(
              decoration: InputDecoration(labelText: 'Description'),
              onSaved: (value) {
                _description = value!;
              },
            ),
            RaisedButton(
              onPressed: _submit,
              child: Text('Ajouter'),
            ),
          ],
        ),
      ),
    ),
    );
  }
}
