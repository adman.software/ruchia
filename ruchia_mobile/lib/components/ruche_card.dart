import 'package:flutter/material.dart';
import 'package:ruchia_mobile/models/ruche.dart';
import 'package:ruchia_mobile/services/ruche_service.dart';

class RucheCard extends StatefulWidget {
   final Ruche ruche;
   final Function(String) onDeleteRuche;

  const RucheCard({
    required this.ruche, 
    required this.onDeleteRuche,
    Key? key,
    }) : super(key: key);

  @override
  _RucheCardState createState() => _RucheCardState();
}

class _RucheCardState extends State<RucheCard> {
  late Ruche ruche;

  @override
  void initState() {
    super.initState();
    ruche = widget.ruche;
  }

  void toggleAlerteOuverture(bool value) async {
    ruche.alerte_ouverture = value;
    final resp = await RucheService().updateRuche(ruche.id, ruche);
    if (resp.isError) {
      return;
    }

    setState(() {
      ruche = ruche;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  ruche.nom,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () {
                    widget.onDeleteRuche(ruche.id);
                  },
                ),
              ],
            ),
            SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text('Température: ', style: TextStyle(fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                        Text('${ruche.temperature}°C'),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text('Humidité: ', style: TextStyle(fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                        Text('${ruche.humidite}'),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text('Couvercle: ', style: TextStyle(fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                        Text(ruche.couvercle_ferme ? 'Fermé' : 'Ouvert'),
                      ],
                    ),
              ],
            ),
            Row(
              children: [
                Checkbox(
                  value: ruche.alerte_ouverture,
                  onChanged: (value) {
                    toggleAlerteOuverture(value!);
                  },
                ),
                Text('Activer les alertes'),
              ],
            )
          ],
        ),
      ),
    );
  }
}
