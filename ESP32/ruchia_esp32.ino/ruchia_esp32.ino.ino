#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <Arduino.h>
#if defined(ESP32)
  #include <WiFi.h>
#elif defined(ESP8266)
  #include <ESP8266WiFi.h>
#endif
#include <Firebase_ESP_Client.h>
#include <ESP_Mail_Client.h>

//Provide the token generation process info.
#include "addons/TokenHelper.h"
//Provide the RTDB payload printing info and other helper functions.
#include "addons/RTDBHelper.h"

// Just test touch pin - Touch0 is T0 which is on GPIO 4.
const int LED = 2;
int lastTouchVal = 85;
bool isOpen = false;

#define DHTPIN 27     // Digital pin connected to the DHT sensor
#define DHTTYPE    DHT11     // boitier DHT 11 

DHT dht(DHTPIN, DHTTYPE); // déclarer un objet dht11 data (ou OUT) est sur la broche 27

const int buttonPin = 4;  // the number of the pushbutton pin
const int ledPin =  2;    // the number of the LED pin

// variable for storing the pushbutton status 
int buttonState = 0;

// Insert your network credentials
#define WIFI_SSID "PC_ADAM 2373"
#define WIFI_PASSWORD "Imie3333"

// Insert Firebase project API Key
#define API_KEY "AIzaSyD4EETpJn3RcrGzTskkKUESFdr4l8RJFWM"

// Insert RTDB URLefine the RTDB URL */
#define DATABASE_URL "https://ruchia-42e01-default-rtdb.europe-west1.firebasedatabase.app/" 
//Define Firebase Data object
FirebaseData fbdo;

FirebaseAuth auth;
FirebaseConfig config;

// SMTP CONFIG
#define SMTP_HOST "smtp-adman-software-fr.alwaysdata.net"
#define SMTP_PORT 465
#define AUTHOR_EMAIL "adman-software-fr@alwaysdata.net"
#define AUTHOR_PASSWORD "atmaniou"

/* The SMTP Session object used for Email sending */
SMTPSession smtp;

/* Callback function to get the Email sending status */
void smtpCallback(SMTP_Status status);

unsigned long sendDataPrevMillis = 0;
int count = 0;
bool signupOK = false;
String mac_adress;

String ownerEmail;
bool alerte_ouverture = false;

String rucheDoc;
String apiculteurDoc = "apiculteur/";

String getMACAddress() {
  uint8_t mac[6];
  char macStr[18] = {0};
  
  // Récupération de l'adresse MAC
  esp_read_mac(mac, ESP_MAC_WIFI_STA);
  
  // Conversion en chaîne de caractères
  snprintf(macStr, sizeof(macStr), "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
  
  return String(macStr);
}

String readDHTTemperature() {
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Check if any reads failed and exit early (to try again).
  if (isnan(t)) {    
    Serial.println("Failed to read from DHT sensor!");
    return "--";
  }
  else {
    //Serial.println(t);
    return String(t);
  }
}

String readDHTHumidity() {
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  if (isnan(h)) {
    Serial.println("Failed to read from DHT sensor!");
    return "--";
  }
  else {
    //Serial.println(h);
    return String(h);
  }
}

void initRucheInfo () {
  if (signupOK == true) {
    String ownerId;
    if (Firebase.RTDB.getString(&fbdo, rucheDoc + "/apiculteurId")) {
      if (fbdo.dataType() == "string") {
        ownerId = fbdo.stringData();
      }
    }

    if (Firebase.RTDB.getBool(&fbdo, rucheDoc + "/alerte_ouverture")) {
      if (fbdo.dataType() == "boolean") {
        alerte_ouverture = fbdo.boolData();
      }
    }

    if (ownerId.length() > 0 && Firebase.RTDB.getString(&fbdo, apiculteurDoc + ownerId + "/email")) {      
      if (fbdo.dataType() == "string") {
        ownerEmail = fbdo.stringData();
      }
    }    
  }
}

void setup(){
  mac_adress = getMACAddress(); 
  rucheDoc = "ruche/" + mac_adress;
     
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
  dht.begin();  
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("Connecting to Wi-Fi");
  while (WiFi.status() != WL_CONNECTED){
    Serial.print(".");
    delay(300);
  }
  Serial.println();
  Serial.print("Connected with IP: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  /* Assign the api key (required) */
  config.api_key = API_KEY;

  /* Assign the RTDB URL (required) */
  config.database_url = DATABASE_URL;

  /* Sign up */
  if (Firebase.signUp(&config, &auth, "", "")){
    Serial.println("ok");
    signupOK = true;
  }
  else{
    Serial.printf("%s\n", config.signer.signupError.message.c_str());
  }

  /* Assign the callback function for the long running token generation task */
  config.token_status_callback = tokenStatusCallback; //see addons/TokenHelper.h
  
  Firebase.begin(&config, &auth);
  Firebase.reconnectWiFi(true);

  // EMAIL
/** Enable the debug via Serial port
   * none debug or 0
   * basic debug or 1
  */
  smtp.debug(1);

  /* Set the callback function to get the sending results */
  smtp.callback(smtpCallback);

  /* Declare the session config data */
  ESP_Mail_Session session;

  /* Set the session config */
  session.server.host_name = SMTP_HOST;
  session.server.port = SMTP_PORT;
  session.login.email = AUTHOR_EMAIL;
  session.login.password = AUTHOR_PASSWORD;
  session.login.user_domain = "";  

  if (Firebase.ready() && signupOK) {        
    Firebase.RTDB.setString(&fbdo, rucheDoc + "/id", mac_adress);
  }
}

void loop(){
  if (Firebase.ready() && signupOK && (millis() - sendDataPrevMillis > 15000 || sendDataPrevMillis == 0)){
    sendDataPrevMillis = millis();

    if(Firebase.RTDB.setString(&fbdo, rucheDoc + "/temperature", readDHTTemperature())) {
      Serial.println("temperature set");
    }   

    if(Firebase.RTDB.setString(&fbdo, rucheDoc + "/humidite", readDHTHumidity())) {
      Serial.println("humidite set");
    } 

    initRucheInfo();
  }

  if (isPushedDoor()) {
    isOpen = !isOpen;
    if(Firebase.RTDB.setBool(&fbdo, rucheDoc + "/couvercle_ferme", !isOpen)) {
      Serial.println("Couvercle : " + !isOpen);
      if (!isOpen) {
        sendCouvercleOuvertEmail();       
      }
    } 
  }      
  
  delay(500);
}

bool isPushedDoor () {
  const int val = touchRead(T3);
  const int result = lastTouchVal > 75 && val < 75;
  lastTouchVal = val;
  return result;
}

void sendCouvercleOuvertEmail () {
  if (alerte_ouverture == true && ownerEmail.length() > 0) {
  /* Declare the message class */
    SMTP_Message message;    

    /* Set the message headers */
    message.sender.name = "ESP";
    message.sender.email = AUTHOR_EMAIL;
    message.subject = "ESP Test Email";
    message.addRecipient("Espuser", ownerEmail);

    /*Send HTML message*/
    String htmlMsg = "<div style=\"color:#2f4468;\">Votre ruche \"" + mac_adress + "\" a été ouverte !</div>";
    message.html.content = htmlMsg.c_str();
    message.html.content = htmlMsg.c_str();
    message.text.charSet = "us-ascii";
    message.html.transfer_encoding = Content_Transfer_Encoding::enc_7bit;

  }
}

/* Callback function to get the Email sending status */
void smtpCallback(SMTP_Status status){
  /* Print the current status */
  Serial.println(status.info());

  /* Print the sending result */
  if (status.success()){
    Serial.println("----------------");
    ESP_MAIL_PRINTF("Message sent success: %d\n", status.completedCount());
    ESP_MAIL_PRINTF("Message sent failled: %d\n", status.failedCount());
    Serial.println("----------------\n");
    struct tm dt;

    for (size_t i = 0; i < smtp.sendingResult.size(); i++){
      /* Get the result item */
      SMTP_Result result = smtp.sendingResult.getItem(i);
      time_t ts = (time_t)result.timestamp;
      localtime_r(&ts, &dt);

      ESP_MAIL_PRINTF("Message No: %d\n", i + 1);
      ESP_MAIL_PRINTF("Status: %s\n", result.completed ? "success" : "failed");
      ESP_MAIL_PRINTF("Date/Time: %d/%d/%d %d:%d:%d\n", dt.tm_year + 1900, dt.tm_mon + 1, dt.tm_mday, dt.tm_hour, dt.tm_min, dt.tm_sec);
      ESP_MAIL_PRINTF("Recipient: %s\n", result.recipients.c_str());
      ESP_MAIL_PRINTF("Subject: %s\n", result.subject.c_str());
    }
    Serial.println("----------------\n");
  }
}